﻿using OmiLAXR.Utils;
using UnityEditor;

namespace OmiLAXR.Adapters.UnityXR
{
    internal class CustomMenu
    {
        [MenuItem("GameObject / OmiLAXR / Adapters / MRTK Adapter")]
        private static void AddSteamVRAdapter()
            => EditorUtils.AddPrefabToSelection("Prefabs/MRTK_Adapter");
    }
}

﻿using UnityEngine;

namespace OmiLAXR.Adapters.MRTK
{
    public static class GameObject_Ext
    {
        /// <summary>
        /// Returns the full hierarchy name of the game object.
        /// Source: https://gist.github.com/marcosecchi/e8dc19f27c9ab48bc490cb444488d8e6
        /// </summary>
        /// <param name="go">The game object.</param>
        public static string GetFullName (this GameObject go) {
            string name = go.name;
            while (go.transform.parent != null) {

                go = go.transform.parent.gameObject;
                name = go.name + ">" + name;
            }
            return name;
        }
    }
}
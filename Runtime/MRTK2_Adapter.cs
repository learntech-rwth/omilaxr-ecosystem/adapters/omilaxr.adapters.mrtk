using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

namespace OmiLAXR.Adapters.MRTK
{
    public class MRTK2_Adapter : MonoBehaviour
    {
        [FormerlySerializedAs("objectManipulatorAdapter")] [DoNotSerialize]
        public MRTK_ObjectManipulator_Adapter mrtkObjectManipulatorAdapter;
        [FormerlySerializedAs("interactableAdapter")] [FormerlySerializedAs("buttonHoloLens2Adapter")] [DoNotSerialize]
        public MRTK_Interactable_Adapter mrtkInteractableAdapter;
    
        // Start is called before the first frame update
        void Start()
        {
            mrtkObjectManipulatorAdapter = FindObjectOfType<MRTK_ObjectManipulator_Adapter>();
            mrtkInteractableAdapter = FindObjectOfType<MRTK_Interactable_Adapter>();
        
            MRTKSceneManager.onSceneChanged.AddListener(mrtkObjectManipulatorAdapter.RegisterAllObjectManipulators);
            MRTKSceneManager.onSceneChanged.AddListener(mrtkInteractableAdapter.RegisterAllInteractables);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }

}

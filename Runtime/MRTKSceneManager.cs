using System.Linq;
using System.Threading.Tasks;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SceneSystem;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace OmiLAXR.Adapters.MRTK
{
    public class MRTKSceneManager : MonoBehaviour
    {
        private static IMixedRealitySceneSystem _sceneSystem;
        public static UnityEvent onSceneChanged = new UnityEvent();
        [FormerlySerializedAs("StartupSceneName")] [SerializeField] private string startupSceneName = "Startup Scene";
        // Start is called before the first frame update
        async void Start()
        {
            _sceneSystem = MixedRealityToolkit.Instance.GetService<IMixedRealitySceneSystem>();
            //Load Startup Scene
            await LoadSingleScene(startupSceneName, true); //REVIEW await? Dont think it is necessary
        }

        /// <summary>
        /// Unloads all content scenes then loads only the specified content scene
        /// </summary>
        /// <param name="sceneName"> Content scene that is to be loaded, must exactly match scene name</param>
        /// <param name="snapContentToCamera">Should the MixedRealitySceneContent transform be moved and aligned with the camera?</param>
        public static async Task LoadSingleScene(string sceneName, bool snapContentToCamera = true)
        {
            await _sceneSystem.LoadContent(sceneName, LoadSceneMode.Single);
            onSceneChanged.Invoke();
            
            if (snapContentToCamera)
            {
                Scene newScene = _sceneSystem.GetScene(sceneName);
                GameObject[] rootObjects = newScene.GetRootGameObjects();
                GameObject newSceneContent = rootObjects.FirstOrDefault(rootObject => rootObject.name == "MixedRealitySceneContent");

                if (newSceneContent == null)
                {
                    Debug.LogError("The scene you loaded does not contain a MixedRealitySceneContent");
                    return;
                }

                Transform camTransform = CameraCache.Main.transform;
                newSceneContent.transform.eulerAngles = new Vector3(0,camTransform.rotation.eulerAngles.y,0);
                newSceneContent.transform.position = camTransform.position;
            }
        }

        

    }
}

using System;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using UnityEngine.Events;
using xAPI.Registry;

namespace OmiLAXR.Adapters.MRTK
{
    public class MRTK_Interactable_Adapter : MonoBehaviour
    {
        private Interactable[] interactables;

        private Dictionary<Interactable, UnityAction> onClickCalls = new Dictionary<Interactable, UnityAction>();

        // Start is called before the first frame update
        void Start()
        {
            RegisterAllInteractables();
        }

        /// <summary>
        /// Registers all Interactable components for tracking, can be called at any time to update the list
        /// </summary>
        public void RegisterAllInteractables()
        {
            //If called to update list, remove all tracked Interactable components first
            if (interactables != null)
            {
                DeRegisterAllInteractables();
            }

            //Find all Interactables
            interactables =
                (FindObjectsByType(typeof(Interactable), FindObjectsInactive.Include, FindObjectsSortMode.None)) as
                Interactable[];

            if (interactables == null)
            {
                Debug.LogWarning("No Interactable Components found to track");
                return;
            }

            //Add Listeners to all relevant events
            foreach (var interactable in interactables)
            {
                UnityAction action = () =>
                {
                    var iname = interactable.gameObject.GetFullName();
                    Debug.Log("Clicked " + iname);
                    LrsController.Instance.SendStatement(
                        verb: xAPI_Definitions.mixedReality.verbs.interacted,
                        activity: xAPI_Definitions.mixedReality.activities.mrObject,
                        activityExtensions: xAPI_Definitions.mixedReality.extensions.activity.mrObjectName(iname)
                    );
                };
                onClickCalls.Add(interactable, action);
                interactable.OnClick.AddListener(action);
            }

            Debug.Log("Registered all Interactable for tracking");
        }

        /// <summary>
        /// Removes all currently registered Interactables from tracking
        /// </summary>
        public void DeRegisterAllInteractables()
        {
            if (interactables == null)
            {
                Debug.LogWarning("No Interactable found to deregister");
                return;
            }

            foreach (var (interactable, action) in onClickCalls)
            {
                try
                {
                    interactable.OnClick.RemoveListener(action);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            onClickCalls.Clear();
        }
    }
}
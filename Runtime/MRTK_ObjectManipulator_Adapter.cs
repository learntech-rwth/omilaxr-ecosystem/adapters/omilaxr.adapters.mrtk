using System;
using Microsoft.MixedReality.Toolkit.UI;
using OmiLAXR.Adapters.MRTK;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;

public class MRTK_ObjectManipulator_Adapter : MonoBehaviour
{

    private ObjectManipulator[] objectManipulators;
    // Start is called before the first frame update
    void Start()
    {
        RegisterAllObjectManipulators();
            
    }

    /// <summary>
    /// Registers all ObjectManipulator components for tracking, can be called at any time to update the list
    /// </summary>
    public void RegisterAllObjectManipulators()
    {
        //If called to update list, remove all tracked ObjectManipulators first
        if (objectManipulators != null)
        {
            DeRegisterAllTrackedObjectManipulators();
        }
        
        //Find all ObjectManipulators
        objectManipulators = (FindObjectsByType(typeof(ObjectManipulator), FindObjectsInactive.Include, FindObjectsSortMode.None)) as ObjectManipulator[];

        if (objectManipulators == null)
        {
            Debug.LogWarning("No ObjectManipulator Components found to track");
            return;
        }

        //Add Listeners to all relevant events
        foreach (var objectManipulator in objectManipulators)
        {
            objectManipulator.OnHoverEntered.AddListener(OnHoverEntered);
            objectManipulator.OnHoverExited.AddListener(OnHoverExited);
            objectManipulator.OnManipulationStarted.AddListener(OnManipulationStarted);
            objectManipulator.OnManipulationEnded.AddListener(OnManipulationEnded);
        }
        Debug.Log("Registered all ObjectManipulators for tracking");
    }

    /// <summary>
    /// Removes all currently registered ObjectManipulators from tracking
    /// </summary>
    public void DeRegisterAllTrackedObjectManipulators()
    {
        if (objectManipulators == null)
        {
            Debug.LogWarning("No ObjectManipulators found to deregister");
            return;
        }

        foreach (var objectManipulator in objectManipulators)
        {
            try
            {
                objectManipulator.OnHoverEntered.RemoveListener(OnHoverEntered);
                objectManipulator.OnHoverExited.RemoveListener(OnHoverExited);
                objectManipulator.OnManipulationStarted.RemoveListener(OnManipulationStarted);
                objectManipulator.OnManipulationEnded.RemoveListener(OnManipulationEnded);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
        }
    }

    private void OnHoverEntered(ManipulationEventData eventData)
    {
        Debug.Log($"Learner started hovering over {eventData.ManipulationSource.GetFullName()}");
        
    }
    
    private void OnHoverExited(ManipulationEventData eventData)
    {
        Debug.Log($"Learner ended hovering over {eventData.ManipulationSource.GetFullName()}");
    }
    
    private void OnManipulationStarted(ManipulationEventData eventData)
    {
        Debug.Log($"Learner started manipulating {eventData.ManipulationSource.GetFullName()}");
    }
    
    private void OnManipulationEnded(ManipulationEventData eventData)
    {
        Debug.Log($"Learner stopped manipulating {eventData.ManipulationSource.GetFullName()}");
    }
}
